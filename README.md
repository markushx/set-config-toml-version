# set-config-toml-version

Tool that sets the version in Config.toml to a value provided as CLI argument

## Example

```shell
set-config-toml-version 0.1.1
```
