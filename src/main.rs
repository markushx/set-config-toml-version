use std::fs::OpenOptions;
use std::io::{prelude::*, Seek, SeekFrom};

use std::env;
use toml_edit::*;

const MANIFEST_FILENAME: &str = "Cargo.toml";

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(MANIFEST_FILENAME)
        .unwrap();

    let mut data = String::new();
    file.read_to_string(&mut data).unwrap();

    let mut doc = data.parse::<Document>().expect("invalid doc");

    doc["package"]["version"] = value(args[1].clone());

    let s = doc.to_string_in_original_order();
    let new_contents_bytes = s.as_bytes();

    // We need to truncate the file, otherwise the new contents
    // will be mixed up with the old ones.
    file.seek(SeekFrom::Start(0)).unwrap();
    file.set_len(new_contents_bytes.len() as u64).unwrap();
    file.write_all(new_contents_bytes).unwrap();
}
